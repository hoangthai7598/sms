package com.example.sms;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.telephony.SmsMessage;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

public class MyService extends Service {
    SMSReceiver smsReceiver = new SMSReceiver();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startID) {
        registerReceiver(smsReceiver,new IntentFilter("android.provider.Telephony.SMS_DELIVER"));
        //return super.onStartCommand(intent,flags,startID);


        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        registerReceiver(smsReceiver,new IntentFilter("android.provider.Telephony.SMS_DELIVER"));

    }


    private class SMSReceiver  extends BroadcastReceiver {

        //PDU la "protocol data unit" dinh dang cho 1 tin nhan sms
        //public static final String SMS_BUNDLE ="pdus";
        String displayName = "";

        public static final String pdu_type = "pdus";
        @androidx.annotation.RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get the SMS message.
            Bundle bundle = intent.getExtras();
            SmsMessage[] msgs;
            String strMessage = "";
            String Address = "";
            String format = bundle.getString("format");
            // Retrieve the SMS message received.
            Object[] pdus = (Object[]) bundle.get(pdu_type);
            if (pdus != null) {
                // Check the Android version.
                boolean isVersionM =
                        (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M);
                // Fill the msgs array.
                msgs = new SmsMessage[pdus.length];
                for (int i = 0; i < msgs.length; i++) {
                    // Check Android version and use appropriate createFromPdu.
                    if (isVersionM) {
                        // If Android version M or newer:
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i],format);
                    } else
                    {
                        // If Android version L or older:
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    }
                    // Build the message to show.
                    Address += msgs[i].getOriginatingAddress();
                    strMessage +=msgs[i].getMessageBody();
                    // Log and display the SMS message.

                    Toast.makeText(context, strMessage, Toast.LENGTH_LONG).show();
                }

//            if(MainActivity.active)
//            {
//                MainActivity inst = MainActivity.instance();
//                inst.createNotifi();
//            }
//            else
//            {
//                Intent i = new Intent(context,MainActivity.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(i);
//            }

                //test 1
                ChatActivity inst = ChatActivity.instance();
                if(ChatActivity.active)
                    inst.updateMess(Address,strMessage);
                MainActivity inst1 = MainActivity.instance();
                if(MainActivity.active)
                    inst1.updateList(Address,strMessage);

                ContentValues values = new ContentValues();
                values.put("type",1);
                values.put("body",strMessage);
                values.put("address",Address);
                values.put("date",String.valueOf(System.currentTimeMillis()));
                inst1.Insert(values);


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    context.startForegroundService(intent);
                } else {
                    context.startService(intent);
                }


                //dat notification 2020
                NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                        NOTIFICATION_SERVICE
                );
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    NotificationChannel channel = new NotificationChannel("YOUR_CHANNEL_ID",
                            "YOUR_CHANNEL_NAME",
                            NotificationManager.IMPORTANCE_DEFAULT);
                    channel.setDescription("YOUR_NOTIFICATION_CHANNEL_DESCRIPTION");
                    notificationManager.createNotificationChannel(channel);
                }
                NotificationCompat.Builder builder = new NotificationCompat.Builder(
                        context,"YOUR_CHANNEL_ID"
                )
                        .setSmallIcon(R.drawable.ic_message)
                        .setContentTitle(Address)
                        .setContentText(strMessage)
                        .setAutoCancel(true);

                displayName = getContactName(context.getApplicationContext(),Address);
                Intent intent1 = new Intent(context.getApplicationContext(), ChatActivity.class);
                intent1.putExtra("address",Address);
                intent1.putExtra("displayName",displayName);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                PendingIntent pendingIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

                builder.setContentIntent(pendingIntent);


                notificationManager.notify(0, builder.build());
            }
        }
    }
    public String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = "none";
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactName;
    }
}
