package com.example.sms.Adapter;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.SmsMessage;
import android.text.format.DateFormat;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sms.AccountActivity;
import com.example.sms.NewMsgActivity;
import com.example.sms.R;
import com.example.sms.ZoomActivity;
import com.example.sms.models.Messages;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder>  implements Filterable {
    private static final int MSG_TYPE_LEFT = 0;
    private static final int MSG_TYPE_RIGHT = 1;

    Context context;
    List<Messages> messagesList;
    List<Messages> messagesListAll;

    public ChatAdapter(Context context, List<Messages> messagesList) {
        this.context = context;
        this.messagesList = messagesList;
        this.messagesListAll = new ArrayList<>(messagesList);
    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(viewType == MSG_TYPE_LEFT)
        {
            View view = inflater.inflate(R.layout.chat_left, parent, false);
            ChatViewHolder chatViewHolder = new ChatViewHolder(view);
            return chatViewHolder;
        }
        else
        {
            View view = inflater.inflate(R.layout.chat_right, parent, false);
            ChatViewHolder chatViewHolder = new ChatViewHolder(view);
            return chatViewHolder;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder holder, int position)
    {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(Long.parseLong(messagesList.get(position).getDate()));
        String dateTime = DateFormat.format("dd/MM/yyyy hh:mm ", cal).toString();
        holder.txt_time.setText(dateTime);
        holder.txt_message.setText(messagesList.get(position).getBody());

    }


    @Override
    public int getItemCount() {
        return messagesList.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {

        //run on background thread
        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            List<Messages> filteredlist = new ArrayList<>();

            if(constraint.toString().isEmpty()){
                filteredlist.addAll(messagesListAll);
            }
            else{
                for(Messages messages:messagesListAll)
                {
                    if(messages.getBody().toLowerCase().contains(constraint.toString().toLowerCase()))
                    {
                        filteredlist.add(messages);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredlist;

            return filterResults;
        }

        //runs on a ui thread
        @Override
        protected void publishResults(CharSequence constraint, FilterResults filterResults) {
            messagesList.clear();
            messagesList.addAll((Collection<? extends Messages>) filterResults.values);
            notifyDataSetChanged();
        }
    };


    public class ChatViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        TextView txt_message,txt_time;
        public ChatViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_time = itemView.findViewById(R.id.txt_time);
            txt_message = itemView.findViewById(R.id.txt_message);

            itemView.setOnCreateContextMenuListener(this);
        }
        @Override
        public void onCreateContextMenu(final ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.add("Chuyển tiếp").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    //Toast.makeText(context, "cặc", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, NewMsgActivity.class);
                    intent.putExtra("message", txt_message.getText().toString());
                    //Toast.makeText(context, txt_message.getText(), Toast.LENGTH_SHORT).show();
                    context.startActivity(intent);
                    return true;
                }
            });
            contextMenu.add("Sao chép").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem)
                {
                    ClipboardManager clipboard = (ClipboardManager)
                            context.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("simple text", txt_message.getText());
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(context, "Copy", Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
            contextMenu.add("Phóng To").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    //Toast.makeText(context, "Xóa", Toast.LENGTH_SHORT).show();
                    //ContentResolver contentResolver = context.getContentResolver();
                    //Cursor smsInboxCursor = contentResolver.query(Uri.parse("content://sms"), null, null, null, null);
                    //context.getContentResolver().delete(Uri.parse("content://sms/inbox"),"body = ?",new String[] {txt_message.getText().toString()});
                    Intent intent = new Intent(context, ZoomActivity.class);
                    intent.putExtra("mess",txt_message.getText().toString());
                    context.startActivity(intent);
                    return true;
                }
            });
            contextMenu.add("Xóa").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    AlertDialog.Builder dialogXoa = new AlertDialog.Builder(context);
                    dialogXoa.setMessage("Bạn có muốn xóa tin nhắn này không ?");
                    dialogXoa.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            context.getContentResolver().delete(Uri.parse("content://sms"),"date = ? and body = ?",new String [] {messagesList.get(getLayoutPosition()).getDate(),messagesList.get(getLayoutPosition()).getBody()});
                            removeAt(getLayoutPosition());
                            Toast.makeText(context, "Đã xóa", Toast.LENGTH_SHORT).show();
                        }
                    });
                    dialogXoa.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    dialogXoa.show();
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(messagesList.get(position).getType() == 2){
            return MSG_TYPE_RIGHT;
        }
        else{
            return MSG_TYPE_LEFT;
        }
    }
    public void removeAt(int position) {
        messagesList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, messagesList.size());
    }
}
