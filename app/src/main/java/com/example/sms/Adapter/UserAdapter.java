package com.example.sms.Adapter;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sms.ChatActivity;
import com.example.sms.R;
import com.example.sms.models.Messages;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> implements Filterable
{

    Context context;
    List<Messages> messagesList;
    List<Messages> messagesListAll;

    public UserAdapter(Context context, List<Messages> messagesList)
    {
        this.context = context;
        this.messagesList = messagesList;
        this.messagesListAll = new ArrayList<>(messagesList);
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_user, parent, false);

        UserViewHolder userViewHolder = new UserViewHolder(view);
        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final UserViewHolder holder, final int position) {
        final Messages messages = messagesList.get(position);

        final String displayName = getContactName(context, messages.getAddress());
        final String displayPhoto = getPhoto(context,messages.getAddress());

        if(!displayName.equals("none"))
        {
            holder.txt_name.setText(displayName);
        }
        else
        {
            holder.txt_name.setText(messages.getAddress());
        }

        if(displayPhoto!=null)
        {
            Picasso.get().load(displayPhoto).resize(80,80).into(holder.user_image);
            //holder.user_image.setImageURI(Uri.parse(displayPhoto));
        }
        else{
            holder.user_image.setImageResource(R.drawable.profile_image);
        }


        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(Long.parseLong(messages.getDate()));
        String dateTime = DateFormat.format("dd/MM/yyyy hh:mm ", cal).toString();
        holder.txt_time.setText(dateTime);

        holder.txt_last_msg.setText(messages.getBody());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("address", messages.getAddress());
                intent.putExtra("displayName", displayName);
                context.startActivity(intent);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view)
            {
//                ContentResolver contentResolver = context.getContentResolver();
//                int cursor = contentResolver.delete(Uri.parse("content://sms/inbox"),"address = ?",new String[]{messages.getAddress()});
//                ContentResolver contentResolver = context.getContentResolver();
//                Cursor smsInboxCursor = contentResolver.query(Uri.parse("content://sms"), null, null, null, null);
//                context.getContentResolver().delete(Uri.parse("content://sms"),"address = ?",new String[] {messages.getAddress()});
                AlertDialog.Builder dialogXoa = new AlertDialog.Builder(context);
                dialogXoa.setMessage("Bạn có muốn xóa tất cả tin nhắn của "+ holder.txt_name.getText() + " không ?");
                dialogXoa.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        context.getContentResolver().delete(Uri.parse("content://sms"),"address = ?",new String [] {messages.getAddress()});
                        removeAt(position);
                        Toast.makeText(context, "Đã xóa", Toast.LENGTH_SHORT).show();
                    }
                });
                dialogXoa.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                dialogXoa.show();
                return true;
            }
        });
    }

    public String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = "none";
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactName;
    }
    public String getPhoto(Context context,String phoneNumber)
    {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[] { ContactsContract.PhoneLookup.PHOTO_URI }, null, null, null);
        if (cursor == null) {
            return null;
        }
        String photo = null;
        if (cursor.moveToFirst()) {
            photo = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.PHOTO_URI));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return photo;
    }


    @Override
    public int getItemCount()
    {
        return messagesList.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }
    Filter filter = new Filter() {

        //run on background thread
        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            List<Messages> filteredlist = new ArrayList<>();

            if(constraint.toString().isEmpty()){
                filteredlist.addAll(messagesListAll);
            }
            else{
                for(Messages messages:messagesListAll)
                {
                    if(messages.getBody().toLowerCase().contains(constraint.toString().toLowerCase())
                            || getContactName(context,messages.getAddress()).toLowerCase().contains(constraint.toString().toLowerCase())
                            || messages.getAddress().toLowerCase().contains((constraint.toString().toLowerCase())))
                    {
                        filteredlist.add(messages);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredlist;

            return filterResults;
        }

        //runs on a ui thread
        @Override
        protected void publishResults(CharSequence constraint, FilterResults filterResults) {
            messagesList.clear();
            messagesList.addAll((Collection<? extends Messages>) filterResults.values);
            notifyDataSetChanged();
        }
    };

    public class UserViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name, txt_time, txt_last_msg;
        CircleImageView user_image;
        public UserViewHolder(@NonNull View itemView)
        {
            super(itemView);

            txt_name = itemView.findViewById(R.id.txt_name);
            txt_time = itemView.findViewById(R.id.txt_time);
            txt_last_msg = itemView.findViewById(R.id.txt_last_msg);
            user_image = itemView.findViewById(R.id.user_image);
        }
    }
    public void removeAt(int position) {
        messagesList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, messagesList.size());
    }
}
