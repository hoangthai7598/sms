package com.example.sms.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sms.ContactsActivity;
import com.example.sms.NewMsgActivity;
import com.example.sms.R;
import com.example.sms.models.Contact;
import com.example.sms.models.Messages;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> implements Filterable {
    Context mContext;
    List<Contact> contactList;
    List<Contact> contactsListAll;

    public ContactAdapter(Context mContext, List<Contact> contactList) {
        this.mContext = mContext;
        this.contactList = contactList;
        this.contactsListAll = new ArrayList<>(contactList);
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_contactss,parent,false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, final int position) {

        final Contact contact =contactList.get(position);
        holder.name_contact.setText(contact.getName());
        holder.phone_contact.setText(contact.getPhone());

        if(contact.getPhoto()!=null)
        {
            Picasso.get().load(contact.getPhoto()).into(holder.img_contact);
            //holder.img_contact.setImageURI(Uri.parse(contact.getPhoto()));
        }
        else{
            holder.img_contact.setImageResource(R.drawable.profile_image);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent resultIntent = new Intent()  ;
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                resultIntent.putExtra("Phone", contact.getPhone());
                ((Activity)mContext).setResult(Activity.RESULT_OK,resultIntent);
                ((Activity)mContext).finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }
    Filter filter = new Filter() {
        //run on background
        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            List<Contact> filteredlist = new ArrayList<>();

            if(constraint.toString().isEmpty()){
                filteredlist.addAll(contactsListAll);
            }
            else{
                for(Contact contact:contactsListAll)
                {
                    if(contact.getName().toLowerCase().contains(constraint.toString().toLowerCase()))
                    {
                        filteredlist.add(contact);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredlist;

            return filterResults;
        }

        //run on UI
        @Override
        protected void publishResults(CharSequence constraint, FilterResults filterResults) {

            contactList.clear();
            contactList.addAll((Collection<? extends Contact>) filterResults.values);
            notifyDataSetChanged();
        }
    };

    public class ContactViewHolder extends RecyclerView.ViewHolder
    {
        TextView name_contact,phone_contact;
        CircleImageView img_contact;
        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);
            name_contact = itemView.findViewById(R.id.name_contact);
            phone_contact = itemView.findViewById(R.id.phone_contact);
            img_contact = itemView.findViewById(R.id.img_contact);
        }
    }
}
