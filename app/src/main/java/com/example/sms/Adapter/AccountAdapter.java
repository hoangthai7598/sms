package com.example.sms.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sms.AccountActivity;
import com.example.sms.MainActivity;
import com.example.sms.R;
import com.example.sms.models.Messages;
import com.example.sms.models.TaiKhoan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AccountAdapter extends BaseAdapter implements Filterable
{

     AccountActivity context;
     int layout;
     List<TaiKhoan> taiKhoanList;
     List<TaiKhoan> taiKhoanListAll;

    public AccountAdapter(AccountActivity context, int layout, List<TaiKhoan> taiKhoanList) {
        this.context = context;
        this.layout = layout;
        this.taiKhoanList = taiKhoanList;
        this.taiKhoanListAll = new ArrayList<>(taiKhoanList);
    }


    @Override
    public int getCount() {
        return taiKhoanList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }
    Filter filter = new Filter() {

        //run on background thread
        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            List<TaiKhoan> filteredlist = new ArrayList<>();

            if(constraint.toString().isEmpty()){
                filteredlist.addAll(taiKhoanListAll);
            }
            else{
                for(TaiKhoan taiKhoan:taiKhoanListAll)
                {
                    if(taiKhoan.getTenTK().toLowerCase().contains(constraint.toString().toLowerCase()) || taiKhoan.getTenNH().toLowerCase().contains(constraint.toString().toLowerCase()))
                    {
                        filteredlist.add(taiKhoan);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredlist;

            return filterResults;
        }

        //runs on a ui thread
        @Override
        protected void publishResults(CharSequence constraint, FilterResults filterResults) {
            taiKhoanList.clear();
            taiKhoanList.addAll((Collection<? extends TaiKhoan>) filterResults.values);
            notifyDataSetChanged();
        }
    };

    private class ViewHolder
    {
        TextView txtTK,txtNH,txtTien;
        ImageView imgDelete,imgEdit;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView== null )
        {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layout,null);
            holder.txtTK = (TextView)convertView.findViewById(R.id.txt_tk);
            holder.txtNH = (TextView)convertView.findViewById(R.id.txt_nh);
            holder.txtTien = (TextView)convertView.findViewById(R.id.txt_tien);
            holder.imgDelete = (ImageView)convertView.findViewById(R.id.imgDelete);
            holder.imgEdit = (ImageView)convertView.findViewById(R.id.imgEdit);
            convertView.setTag(holder);
        }else
            {
                holder = (ViewHolder) convertView.getTag();
            }

        final TaiKhoan taiKhoan = taiKhoanList.get(position);

        holder.txtTK.setText(taiKhoan.getTenTK());
        holder.txtTien.setText(taiKhoan.getTien());
        holder.txtNH.setText(taiKhoan.getTenNH());

        //bắt sự kiện xóa & sửa
        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.DialogSuaCongViec(taiKhoan.getTenTK(),taiKhoan.getTenNH(),taiKhoan.getTien(),taiKhoan.getIdTK());
            }
        });

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.DialogXoaCV(taiKhoan.getTenTK(),taiKhoan.getIdTK());
            }
        });

        return convertView;
    }
}
