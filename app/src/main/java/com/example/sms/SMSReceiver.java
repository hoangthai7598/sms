package com.example.sms;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.sms.Database.Database;

import java.text.SimpleDateFormat;

import static android.content.Context.NOTIFICATION_SERVICE;

public class SMSReceiver extends BroadcastReceiver {

    //PDU la "protocol data unit" dinh dang cho 1 tin nhan sms
    //public static final String SMS_BUNDLE ="pdus";
    Database database;
    private static final String TAG =
            SMSReceiver.class.getSimpleName();
    String displayName="";
    public static final String pdu_type = "pdus";
    @androidx.annotation.RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
        // Get the SMS message.
        Bundle bundle = intent.getExtras();
        SmsMessage[] msgs;
        String strMessage = "";
        String Address ="";
        String format = bundle.getString("format");
        // Retrieve the SMS message received.
        Object[] pdus = (Object[]) bundle.get(pdu_type);
        if (pdus != null) {
            // Check the Android version.
            boolean isVersionM =
                    (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M);
            // Fill the msgs array.
            msgs = new SmsMessage[pdus.length];
            for (int i = 0; i < msgs.length; i++) {
                // Check Android version and use appropriate createFromPdu.
                if (isVersionM) {
                    // If Android version M or newer:
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i],format);
                } else
                    {
                    // If Android version L or older:
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                }
                // Build the message to show.
                strMessage +=msgs[i].getMessageBody();
                Address += msgs[i].getOriginatingAddress();
                // Log and display the SMS message.
                Log.d(TAG, "onReceive: " + strMessage);
                Toast.makeText(context, strMessage, Toast.LENGTH_LONG).show();
            }

//            if(MainActivity.active)
//            {
//                MainActivity inst = MainActivity.instance();
//                inst.createNotifi();
//            }
//            else
//            {
//                Intent i = new Intent(context,MainActivity.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(i);
//            }




            //test 1
            ChatActivity inst = ChatActivity.instance();
            if(ChatActivity.active)
            {
                inst.updateMess(Address, strMessage);
            }
            MainActivity inst1 = MainActivity.instance();
            if(MainActivity.active) {
                inst1.updateList(Address, strMessage);
            }

            ContentValues values = new ContentValues();
            values.put("type",1);
            values.put("body",strMessage);
            values.put("address",Address);
            values.put("date",String.valueOf(System.currentTimeMillis()));
            inst1.Insert(values);

            //update in data
            AccountActivity inst2 = AccountActivity.instance();
            if(Address.toLowerCase().trim().contains("bank"))
            {
                if(strMessage.toString().contains("SD TK"))
                {
                    //database.QueryData("UPDATE TaiKhoan SET Tien= '"+addChar(removeChar(checkTien(strMessage)),',')+"' WHERE TenTk = '" + checkTK(strMessage) + "'");
                    database.QueryData("UPDATE TaiKhoan SET Tien= '"+checkTien(strMessage)+"' WHERE TenTk = '" + checkTK(strMessage) + "'");
                    if(AccountActivity.active)
                        inst2.GetDataTK();
                }
            }
            displayName = getContactName(context.getApplicationContext(),Address);
            if( Address.contains("+84343934545"))
            {
                if(strMessage.toString().contains("SD TK"))
                {
                    //database.QueryData("UPDATE TaiKhoan SET Tien= '"+addChar(removeChar(checkTien(strMessage)),',')+"' WHERE TenTk = '" + checkTK(strMessage) + "'");
                    database.QueryData("UPDATE TaiKhoan SET Tien= '"+checkTien(strMessage)+"' WHERE TenTk = '" + checkTK(strMessage) + "'");
                    if(AccountActivity.active)
                        inst2.GetDataTK();
                }
            }

          //dat notification 2020
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    NOTIFICATION_SERVICE
            );
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel("YOUR_CHANNEL_ID",
                        "YOUR_CHANNEL_NAME",
                        NotificationManager.IMPORTANCE_DEFAULT);
                channel.setDescription("YOUR_NOTIFICATION_CHANNEL_DESCRIPTION");
                notificationManager.createNotificationChannel(channel);
            }
            NotificationCompat.Builder builder = new NotificationCompat.Builder(
                    context,"YOUR_CHANNEL_ID"
            )
                    .setSmallIcon(R.drawable.ic_message)
                    .setContentTitle(Address)
                    .setContentText(strMessage)
                    .setAutoCancel(true);

            displayName = getContactName(context.getApplicationContext(),Address);
            Intent intent1 = new Intent(context.getApplicationContext(), ChatActivity.class);
            intent1.putExtra("address",Address);
            intent1.putExtra("displayName",displayName);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

            builder.setContentIntent(pendingIntent);


            notificationManager.notify(0, builder.build());
        }
    }
    public String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = "none";
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactName;
    }
    public String getTK(String tenTk)
    {
        return tenTk;
    }


    //them ky tu va xoa dau ,
    public String addChar(String str, char ch) {
        StringBuilder sb = new StringBuilder(str);
        int a = str.length();
        while(a>0)
        {
            a = a-3;
            if(a>0)
            {sb.insert(a,',');}
        }
        return sb.toString();
    }
    public String removeChar(String str)
    {
        return str.replaceAll(",","");
    }
    public String checkTK(String s)
    {
        String[] arrOfStr = s.split("TK ", -3);
        String[] a = arrOfStr[1].split(" ",-3);
        System.out.println(a[0]);
        return a[0];
    }
    public String checkTien(String s)
    {
        String[] arrOfStr = s.split("TK ", -3);
        String[] a = arrOfStr[1].split("SD ",-3);
        String[] b = a[1].split("VND.");
        return b[0];
    }
}
