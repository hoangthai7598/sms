package com.example.sms;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sms.Adapter.AccountAdapter;
import com.example.sms.Database.Database;
import com.example.sms.models.TaiKhoan;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class AccountActivity extends AppCompatActivity {

    Database database;
    ListView lvTaiKhoan;
    ArrayList<TaiKhoan> arrayTK;
    AccountAdapter adapter;
    FloatingActionButton btnThem;
    private static AccountActivity inst1;
    public static boolean active = false;

    public static AccountActivity instance()
    {
        return inst1;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("Tài khoản của tôi");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        lvTaiKhoan = (ListView)findViewById(R.id.list_account);
        final EditText search_txt = (EditText)findViewById(R.id.search_btn);
        arrayTK = new ArrayList<>();

        adapter = new AccountAdapter(this,R.layout.dong_tai_khoan,arrayTK);
        //tao database taikhoan
        database = new Database(this,"taikhoan.sqlite", null,1);
        lvTaiKhoan.setAdapter(adapter);

        //tao bang Tai Khoan
        database.QueryData("CREATE TABLE IF NOT EXISTS TaiKhoan(Id INTEGER PRIMARY KEY AUTOINCREMENT, TenTK VARCHAR(200), TenNH VARCHAR(200),Tien VARCHAR(200) )");

        if(arrayTK.isEmpty())
        {
            AlertDialog.Builder dialogXoa = new AlertDialog.Builder(this);
            dialogXoa.setMessage("Bạn chưa lưu tài khoản, hãy ấn vào nút + để thêm tài khoản ngân hàng nhé");
        }
        GetDataTK();
        //insert data
        //database.QueryData("Delete from TaiKhoan VALUES (null,'So TK:','Ten TK:','So Tien:')");
        inst1 = this;
        search_txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                GetData(search_txt.getText().toString());
                //adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnThem = (FloatingActionButton)findViewById(R.id.menu_them);
        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogThem();
            }
        });
    }

    public void DialogXoaCV(final String tencv, final int id)
    {
        AlertDialog.Builder dialogXoa = new AlertDialog.Builder(this);
        dialogXoa.setMessage("Bạn có muốn xóa tài khoản "+tencv+" không ?");
        dialogXoa.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                database.QueryData("DELETE FROM TaiKhoan WHERE Id ='"+ id +"'");
                Toast.makeText(AccountActivity.this, "Đã xóa tài khoản "+tencv+" !", Toast.LENGTH_SHORT).show();
                GetDataTK();
            }
        });
        dialogXoa.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogXoa.show();
    }

    public void DialogSuaCongViec(String ten, String tenNH, String tien, final int id)
    {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_sua);

        final EditText edtTenTK = (EditText) dialog.findViewById(R.id.tkEdit);
        final EditText edtNH = (EditText) dialog.findViewById(R.id.nhEdit);
        final EditText edtTien = (EditText) dialog.findViewById(R.id.tienEdit);
        Button btSua = (Button) dialog.findViewById(R.id.bt_Sua);
        Button btHuy = (Button) dialog.findViewById(R.id.bt_Huy);

        String tienMoi = edtTien.getText().toString().trim();
        edtTenTK.setText(ten);
        edtNH.setText(tenNH);
        edtTien.setText(removeChar(tien));


        btSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tenMoi = edtTenTK.getText().toString().trim();
                String tenNHMoi = edtNH.getText().toString().trim();
                String tienMoi = edtTien.getText().toString().trim();
                database.QueryData("UPDATE TaiKhoan SET TenTK= '"+tenMoi+"' WHERE Id = '" + id + "'");
                database.QueryData("UPDATE TaiKhoan SET TenNH= '"+tenNHMoi+"' WHERE Id = '" + id + "'");
                database.QueryData("UPDATE TaiKhoan SET Tien= '"+addChar(tienMoi,',')+"' WHERE Id = '" + id + "'");
                Toast.makeText(AccountActivity.this, "Cập nhật thành công", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                GetDataTK();
            }
        });

        btHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void GetData(String timkiem)
    {
        Cursor dataTK = database.GetData("SELECT * FROM TaiKhoan WHERE TenTK LIKE '%"+timkiem+"%' or TenNH LIKE '%"+timkiem+"%'");
        arrayTK.clear();
        while ((dataTK.moveToNext()))
        {
            String ten = dataTK.getString(1);
            String tenNH = dataTK.getString(2);
            String tien = dataTK.getString(3);
            int id = dataTK.getInt(0);
            arrayTK.add(new TaiKhoan(id,ten,tenNH,tien));
        }
        adapter.notifyDataSetChanged();
    }

    public void GetDataTK()
    {
        //select data
        Cursor dataTK = database.GetData("SELECT * FROM TaiKhoan");
        arrayTK.clear();
        while ((dataTK.moveToNext()))
        {
            String ten = dataTK.getString(1);
            String tenNH = dataTK.getString(2);
            String tien = dataTK.getString(3);
            int id = dataTK.getInt(0);
            arrayTK.add(new TaiKhoan(id,ten,tenNH,tien));
        }
        adapter.notifyDataSetChanged();
    }



    private  void DialogThem()
    {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_them);

        final EditText edtTen =(EditText) dialog.findViewById(R.id.tk);
        final EditText edtNH = (EditText) dialog.findViewById(R.id.nh);
        final EditText edtTien = (EditText) dialog.findViewById(R.id.tien);
        Button btnThem = (Button)dialog.findViewById(R.id.btn_Them);
        Button btnHuy = (Button)dialog.findViewById(R.id.btn_Huy);

        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ten = edtTen.getText().toString();
                String tenNH = edtNH.getText().toString();
                String tien = edtTien.getText().toString();
                if(ten.equals(""))
                {
                    Toast.makeText(AccountActivity.this, "Vui lòng nhập số tài khoản!", Toast.LENGTH_SHORT).show();
                }
                else if(tenNH.equals(""))
                {
                    Toast.makeText(AccountActivity.this, "Vui lòng nhập tên ngân hàng!", Toast.LENGTH_SHORT).show();
                }
                else if(tien.equals(""))
                {
                    Toast.makeText(AccountActivity.this, "Vui lòng nhập số tiền!", Toast.LENGTH_SHORT).show();
                }
                else if(!ten.equals("") && !tenNH.equals("") && !tien.equals(""))
                {

                    database.QueryData("INSERT INTO TaiKhoan VALUES(null,'"+ten+"','"+tenNH+"','"+addChar(tien,',')+"')");
                    Toast.makeText(AccountActivity.this, "Đã thêm", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    GetDataTK();
                }
            }
        });

        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public String addChar(String str, char ch) {
        StringBuilder sb = new StringBuilder(str);
        int a = str.length();
        while(a>0)
        {
            a = a-3;
            if(a>0)
            {sb.insert(a,',');}
        }
        return sb.toString();
    }
    public String removeChar(String str)
    {
        return str.replaceAll(",","");
    }

    @Override
    protected void onPause() {
        super.onPause();
        active = false;
    }

    @Override
    protected void onStart() {
        GetDataTK();
        super.onStart();
        active = true;
    }
}
