package com.example.sms;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Toast;
import android.Manifest.permission.*;

import com.klinker.android.send_message.ApnUtils;
import com.klinker.android.send_message.Message;
import com.klinker.android.send_message.Transaction;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class NewMsgActivity extends AppCompatActivity {
    EditText txt_phone, txt_chat;
    ImageButton btn_send1;
    ImageButton btn_contact;
    private static String phone;
    ImageButton back;
    private static String message;
    ImageButton btn_pic;
    private Settings settings;

    String tinnhan;
    private static final int MY_PERMISSION_REQUEST_SEND_SMS =0;
    public static final int PICK_IMAGE = 2;

    SeekBar seekBar;
    MediaPlayer mediaPlayer;

    Handler handler;
    Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_msg);
        back = (ImageButton) findViewById(R.id.back);
        initSettings();




        if(ContextCompat.checkSelfPermission(this,Manifest.permission.SEND_SMS)!=PackageManager.PERMISSION_GRANTED)
        {
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.SEND_SMS))
            {

            }
            else
            {
                ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.SEND_SMS},MY_PERMISSION_REQUEST_SEND_SMS);
            }
        }

        btn_pic = findViewById(R.id.btn_pic);
        txt_phone = findViewById(R.id.txt_phone);
        txt_chat = findViewById(R.id.txt_chat);
        btn_send1 = findViewById(R.id.btn_send1);
        btn_contact = findViewById(R.id.contacts);


        btn_send1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                sendTextMsg();
            }
        });

        btn_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewMsgActivity.this,ContactsActivity.class);
                startActivityForResult(intent,1);
            }
        });
                Intent intent = getIntent();
        //phone = intent.getStringExtra("Phone");
        message = intent.getStringExtra("message");
        if(phone!=null)
        {
            txt_phone.setText(phone);
        }
        if(message!=null)
        {
            txt_chat.setText(message);
        }

        if(txt_chat.getText()!=null)
        {
            tinnhan = txt_chat.getText().toString();
        }
        txt_chat.setText(tinnhan);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewMsgActivity.this,MainActivity.class);
                intent.putExtra("mess",message);
                startActivity(intent);
                finish();
            }
        });


        btn_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, String permission[],int[] grantResults)
    {
        //check request code
        switch (requestCode)
        {
            case MY_PERMISSION_REQUEST_SEND_SMS:
                {
                    //check length of result grater than 0 and equal permission granted
                    if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
                    {
                        //Toast.makeText(this, "Access", Toast.LENGTH_SHORT).show();
                    }else{
                        //Toast.makeText(this, "Denied", Toast.LENGTH_SHORT).show();
                    }
                }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1)
        {
            if(resultCode == RESULT_OK)
            {
                final String phone = data.getStringExtra("Phone");
                txt_phone.setText(phone);
            }
            else if(requestCode == RESULT_CANCELED)
            {
                txt_phone.setText("");
            }
        }
        else if(requestCode == PICK_IMAGE)
        {
            if(resultCode == Activity.RESULT_OK)
            {
//                final Bundle extras = data.getExtras();
//                if (extras != null) {
//                    //Get image
//                    Bitmap pic = extras.getParcelable("data");
//                    sendImg(pic);
//                    Toast.makeText(this, "cặc", Toast.LENGTH_SHORT).show();
//                }
                // Let's read picked image data - its URI
                if(data!=null)
                {
                    Uri pickedImage = data.getData();
                    // Let's read picked image path using content resolver
                    String[] filePath = { MediaStore.Images.Media.DATA };
                    Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
                    cursor.moveToFirst();
                    String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);

                    // Do something with the bitmap


                    // At the end remember to close the cursor or you will end with the RuntimeException!
                    cursor.close();
                    sendImg(bitmap);
                }
            }
        }
    }
    // setting
    private void initSettings() {
        settings = Settings.get(this);

        if (TextUtils.isEmpty(settings.getMmsc()) &&
                Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            initApns();
        }
    }
    private void initApns() {
        ApnUtils.initDefaultApns(this, new ApnUtils.OnApnFinishedListener() {
            @Override
            public void onFinished() {
                settings = Settings.get(NewMsgActivity.this, true);
            }
        });
    }

    protected void sendTextMsg()
    {
        String phone = txt_phone.getText().toString().trim();
        String chat = txt_chat.getText().toString().trim();
//        txt_phone.setText("");
        if(!TextUtils.isEmpty(chat) && !TextUtils.isEmpty(phone))
        {
            SmsManager smsManager = SmsManager.getDefault();
            //txt_chat.setText("");
            if(chat.length() >= 150)
            {
                ArrayList<String> parts =smsManager.divideMessage(chat);
                int numParts = parts.size();
                smsManager.sendMultipartTextMessage(phone,null,parts,null,null);
                Toast.makeText(this, "Đã gửi tới "+phone, Toast.LENGTH_SHORT).show();
                txt_chat.setText("");
            }
            else
                {
                    smsManager.sendTextMessage(phone,null,chat,null,null);
                    ContentValues values = new ContentValues();
                    values.put("type",2);
                    values.put("body",chat);
                    values.put("address",phone);
                    values.put("date",String.valueOf(System.currentTimeMillis()));
                    getContentResolver().insert(Uri.parse("content://sms"),values);
                    Toast.makeText(this, "Đã gửi tới "+phone, Toast.LENGTH_SHORT).show();
                    txt_chat.setText("");
                }
        }
        else
            {if(!TextUtils.isEmpty(chat))
                Toast.makeText(this, "Hãy nhập SĐT muốn gửi", Toast.LENGTH_SHORT).show();
                else
                Toast.makeText(this, "Hãy nhập tin nhắn ", Toast.LENGTH_SHORT).show();
            }
    }
    public void sendMessage() {
        final String phone = txt_phone.getText().toString().trim();
        final String address = phone.replaceAll(" ","");
        final String chat = txt_chat.getText().toString();
        if(!TextUtils.isEmpty(chat) && !TextUtils.isEmpty(phone))
        {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    com.klinker.android.send_message.Settings sendSettings = new com.klinker.android.send_message.Settings();
                    sendSettings.setMmsc(settings.getMmsc());
                    sendSettings.setProxy(settings.getMmsProxy());
                    sendSettings.setPort(settings.getMmsPort());
                    sendSettings.setUseSystemSending(true);
                    Transaction transaction = new Transaction(NewMsgActivity.this, sendSettings);

                    Message message = new Message(chat,address);

                    transaction.sendNewMessage(message, Transaction.NO_THREAD_ID);
                    txt_chat.setText("");
                }
            }).start();
            Toast.makeText(this, "đã gửi", Toast.LENGTH_SHORT).show();
        }
        else Toast.makeText(this, "Nhập tin nhắn hoặc SĐT", Toast.LENGTH_SHORT).show();
    }
    public void sendImg(final Bitmap pic)
    {
        final String phone = txt_phone.getText().toString().trim();
        final String address = phone.replaceAll(" ","");
        final String chat = txt_chat.getText().toString();
        if(!TextUtils.isEmpty(phone)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    com.klinker.android.send_message.Settings sendSettings = new com.klinker.android.send_message.Settings();
                    sendSettings.setMmsc(settings.getMmsc());
                    sendSettings.setProxy(settings.getMmsProxy());
                    sendSettings.setPort(settings.getMmsPort());
                    sendSettings.setUseSystemSending(true);
                    Transaction transaction = new Transaction(NewMsgActivity.this, sendSettings);

                    Message message = new Message("", address);
                    message.setImage(pic);
                    transaction.sendNewMessage(message, Transaction.NO_THREAD_ID);
                    txt_chat.setText("");
                }
            }).start();
            txt_chat.setText("");
            Toast.makeText(this, "đã gửi ảnh tới "+phone, Toast.LENGTH_SHORT).show();
        }else Toast.makeText(this, "Nhập SĐT muốn gửi", Toast.LENGTH_SHORT).show();
    }
    public void playCycle(){
        if(mediaPlayer.isPlaying()){
            runnable = new Runnable() {
                @Override
                public void run() {
                    playCycle();
                }
            };
            handler.postDelayed(runnable,1000);
        }
    }
}
