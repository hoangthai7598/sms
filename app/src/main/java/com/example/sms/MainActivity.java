package com.example.sms;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.sms.Adapter.UserAdapter;
import com.example.sms.models.Messages;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    //drawer layout
    private NavigationView navigationView;
    private DrawerLayout dl;
    private ActionBarDrawerToggle abdt;

    //hello
    //String myPackageName = getPackageName();
    Intent smsServiceIntent;
    String[] permission = {Manifest.permission.SEND_SMS, Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_CONTACTS };
    private final int REQUEST_CODE_SEND_SMS = 100;

    RecyclerView recyler_users;
    UserAdapter userAdapter;

    List<Messages> messagesList;
    List<String> nameList;
    List<String> photoList;
    FloatingActionButton addBtn;
    SwipeRefreshLayout refresh;

    private EditText searchtxt;

    private static MainActivity inst;
    public static boolean active = false;


    public static MainActivity instance() {
        return inst;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Intent setSmsAppIntent =
//                new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
//        setSmsAppIntent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME,
//                getPackageName());
//        startActivity(setSmsAppIntent);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#304FFE")));
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        //navigation bar
        dl = (DrawerLayout) findViewById(R.id.dl);
        abdt = new ActionBarDrawerToggle(this,dl,R.string.Open,R.string.Close){
            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        }
        ;
        abdt.setDrawerIndicatorEnabled(true);

        dl.addDrawerListener(abdt);
        abdt.syncState();

        NavigationView nav_view = (NavigationView)findViewById(R.id.nav_view);

        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                if(id== R.id.profile)
                {
                    Intent intent = new Intent(MainActivity.this,AccountActivity.class);
                    startActivity(intent);
                }
                else if(id == R.id.mms)
                {
                    Intent intent = new Intent(MainActivity.this,MMSActivity.class);
                    startActivity(intent);
                }

                return true;
            }
        });

        addBtn = (FloatingActionButton)findViewById(R.id.addmess);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewMsgActivity.class);
                startActivity(intent);
//                Intent intent = new Intent(MainActivity.this, MMSsendActivity.class);
//                startActivity(intent);
            }
        });

        smsServiceIntent = new Intent(MainActivity.this, MyService.class);

        searchtxt =(EditText)findViewById(R.id.search_txt);

        messagesList = new ArrayList<>();
        nameList = new ArrayList<>();
        photoList = new ArrayList<>();
        recyler_users = findViewById(R.id.recyler_users);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyler_users.setLayoutManager(layoutManager);

        refresh = findViewById(R.id.refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh() {
                readSMS();
            }
        });
        inst = this;

        readSMS();

        recyler_users.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);
            }
        });
        searchtxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
//                messagesList.clear();
//                nameList.clear();
//                photoList.clear();
//                ContentResolver contentResolver = getContentResolver();
//                Cursor smsInboxCursor = contentResolver.query(Uri.parse("content://sms/inbox"), null, "person LIKE '%" + searchtxt.getText().toString() + "%' or body LIKE '%"+searchtxt.getText().toString() +"%'"+"or address LIKE '%" + searchtxt.getText().toString() + "%'", null, null);
//                int indexBody = smsInboxCursor.getColumnIndex("body");
//                int indexAddress = smsInboxCursor.getColumnIndex("address");
//                int indexType = smsInboxCursor.getColumnIndex("type");
//                int indexDate = smsInboxCursor.getColumnIndex("date");
//
//                if (indexBody < 0 || !smsInboxCursor.moveToFirst() || indexType < 0) return;
//                do {
//                    int type = smsInboxCursor.getInt(indexType);
//                    String body = smsInboxCursor.getString(indexBody);
//                    String address = smsInboxCursor.getString(indexAddress);
//                    String date = smsInboxCursor.getString(indexDate);
//
//                    Messages messages = new Messages(type, address, body, date);
//                    if (!nameList.contains(messages.getAddress()) && !photoList.contains(messages.getAddress())) {
//                        nameList.add(messages.getAddress());
//                        photoList.add(messages.getAddress());
//                        messagesList.add(messages);
//                    }
//                } while (smsInboxCursor.moveToNext());
//
//                userAdapter = new UserAdapter(MainActivity.this, messagesList);
//                recyler_users.setAdapter(userAdapter);
//                userAdapter.notifyDataSetChanged();
                userAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void readSMS(){
        if( (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS)!=PackageManager.PERMISSION_GRANTED)
        )
        {
            ActivityCompat.requestPermissions(this, permission, REQUEST_CODE_SEND_SMS);
        }
        else {
            messagesList.clear();
            nameList.clear();
            photoList.clear();
            ContentResolver contentResolver = getContentResolver();
            Cursor smsInboxCursor = contentResolver.query(Uri.parse("content://sms"), null, null, null, null);
            int indexBody = smsInboxCursor.getColumnIndex("body");
            int indexAddress = smsInboxCursor.getColumnIndex("address");
            int indexType = smsInboxCursor.getColumnIndex("type");
            int indexDate = smsInboxCursor.getColumnIndex("date");

            if (indexBody < 0 || !smsInboxCursor.moveToFirst() || indexType < 0) return;
            do {
                int type = smsInboxCursor.getInt(indexType);
                String body = smsInboxCursor.getString(indexBody);
                String address = smsInboxCursor.getString(indexAddress);
                String date = smsInboxCursor.getString(indexDate);

                Messages messages = new Messages(type, address, body, date);
                if (!nameList.contains(messages.getAddress()) &&!photoList.contains(messages.getAddress()))
                {
                    nameList.add(messages.getAddress());
                    messagesList.add(messages);
                    photoList.add(messages.getAddress());
                }
            } while (smsInboxCursor.moveToNext());

            userAdapter = new UserAdapter(this, messagesList);
            recyler_users.setAdapter(userAdapter);
            userAdapter.notifyDataSetChanged();
        }

        refresh.setRefreshing(false);
    }
//    public void updateList(String  address,String chat )
//    {
//        Messages newMsg = new Messages(1, address, chat, String.valueOf(System.currentTimeMillis()));
//        messagesList.add(newMsg);
//        recyler_users.notifyDataSetChanged();
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_CODE_SEND_SMS)
        {
            readSMS();
        }
        else
        {
            Toast.makeText(MainActivity.this, "Từ chối!!!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
//        if(item.getItemId() == R.id.it_add) {
//            Intent intent = new Intent(MainActivity.this, NewMsgActivity.class);
//            startActivity(intent);
//        }
        if(item.getItemId() == R.id.ic_account)
        {
            Intent intent = new Intent(MainActivity.this,AccountActivity.class);
            startActivity(intent);
        }
        else if(item.getItemId() == android.R.id.home)
        {
            dl.openDrawer(GravityCompat.START);
            //return true;
        }
        return abdt.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }
    public void createNotifi()
    {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                .setSmallIcon(android.R.drawable.ic_popup_reminder)
                        .setContentTitle("Tin nhắn mới")
                .setContentText("Mời bạn trở lại màn hình chính và cuộn lên trên cùng để xem tin nhắn");

        Intent resultIntent = new Intent(this,MainActivity.class);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        //gán âm thanh
        mBuilder.setContentIntent(resultPendingIntent);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(uri);

        int notificationId = 113;

        NotificationManager mNotify =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotify.notify(notificationId,mBuilder.build());
    }
    public void updateList(String Address, String MessBody)
    {
        Messages newMsg = new Messages(1, Address, MessBody, String.valueOf(System.currentTimeMillis()));
        messagesList.add(newMsg);
        userAdapter.notifyDataSetChanged();
        recyler_users.smoothScrollToPosition(recyler_users.getAdapter().getItemCount());
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        //readSMS();
        active = true;
        //inst = this;

    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        MainActivity.this.startService(smsServiceIntent);
        super.onResume();

        final String myPackageName = getPackageName();
        if (!Telephony.Sms.getDefaultSmsPackage(this).equals(myPackageName)) {
            // App is not default.
            // Show the "not currently set as the default SMS app" interface
                    Intent intent =
                            new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
                    intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME,
                            myPackageName);
                    startActivity(intent);
        }
    }
    public void Insert(ContentValues values)
    {
//        values.put("type",1);
//        values.put("date",String.valueOf(System.currentTimeMillis()));
//        values.put("body",Body);
//        values.put("address",Address);
        Uri uri = getContentResolver().insert(Uri.parse("content://sms"),values);
    }
}
