package com.example.sms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ZoomActivity extends AppCompatActivity {

    TextView mess;
    String text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom);

        Intent intent = getIntent();
        text = intent.getStringExtra("mess");
        mess = (TextView)findViewById(R.id.mess);

        if(text !=null)
        {
            mess.setText(text);
        }
    }
}
