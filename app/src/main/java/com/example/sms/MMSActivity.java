package com.example.sms;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sms.Adapter.MMSAdapter;
import com.example.sms.Adapter.UserAdapter;
import com.example.sms.models.MMSPart;
import com.example.sms.models.Messages;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class MMSActivity extends AppCompatActivity {

    RecyclerView recyclerMMS;
    MMSAdapter mmsAdapter;

    List<MMSPart> messagesList;
    SwipeRefreshLayout refresh;
    FloatingActionButton addMess;
    EditText searchtxt;
    TextView txtCancel;
    UserAdapter userAdapter;
    List<Messages> smsList;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mms);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("MMS");

        //khai bao
        messagesList = new ArrayList<>();
        smsList = new ArrayList<>();
        recyclerMMS = (RecyclerView) findViewById(R.id.mms_activity);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerMMS.setLayoutManager(layoutManager);
        refresh = findViewById(R.id.refreshlayout);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                readallMMS();
            }
        });
        addMess = (FloatingActionButton)findViewById(R.id.addmess);
        addMess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MMSActivity.this, MMSsendActivity.class);
                startActivity(intent);
            }
        });
        readallMMS();
//        txtCancel = (TextView)findViewById(R.id.txtCancel);
//        searchtxt = (EditText)findViewById(R.id.search_txt);
//
//        searchtxt.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    txtCancel.setVisibility(View.VISIBLE);
//                }
//            });
//        txtCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                searchtxt.setText("");
//                txtCancel.setVisibility(View.INVISIBLE);
//            }
//        });
    }
    public void readallMMS()
    {
        ContentResolver contentResolver = getContentResolver();
        final String[] projection = new String[]{"_id", "ct_t"};
        Uri uri = Uri.parse("content://mms-sms/conversations/");
        Cursor query = contentResolver.query(uri, projection, null, null, null);
        messagesList.clear();
        smsList.clear();
        if (query.moveToFirst())
            do {
                int indexId = query.getColumnIndex("_id");
                String string = query.getString(query.getColumnIndex("ct_t"));
                if ("application/vnd.wap.multipart.related".equals(string)) {
                    // it's MMS
                    String mmsId = query.getString(query.getColumnIndex("_id"));
                    readMMS(mmsId);
                } else {
                    // it's SMS
                }
            } while (query.moveToNext());
            mmsAdapter = new MMSAdapter(this, messagesList);
            recyclerMMS.setAdapter(mmsAdapter);
            mmsAdapter.notifyDataSetChanged();

        refresh.setRefreshing(false);
    }
    public void readMMS(String mmsId)
    {
        String selectionPart = "mid=" + mmsId;
        Uri uri = Uri.parse("content://mms/part");
//        Cursor cursor = getContentResolver().query(uri, null,
//                "mid LIKE '%" + mmsId + "%'", null, null);
        Cursor cursor = getContentResolver().query(uri, null,
                selectionPart, null, null);
        if (cursor.moveToFirst()) {
            do {
                String partId = cursor.getString(cursor.getColumnIndex("_id"));
                String type = cursor.getString(cursor.getColumnIndex("ct"));
                MMSPart mmsPart = new MMSPart(partId,type);
                messagesList.add(mmsPart);
//                if ("text/plain".equals(type)) {
//                    String data = cursor.getString(cursor.getColumnIndex("_data"));
//                    String body;
//                    if (data != null) {
//                        // implementation of this method below
//                        body = getMmsText(partId);
//                    } else {
//                        body = cursor.getString(cursor.getColumnIndex("text"));
//                    }
//                }
//                else if ("image/jpeg".equals(type) || "image/bmp".equals(type) ||
//                        "image/gif".equals(type) || "image/jpg".equals(type) ||
//                        "image/png".equals(type)) {
//                    Bitmap bitmap = getMmsImage(partId);
//                }
            } while (cursor.moveToNext());
        }
    }
    public void readSMS(String id)
    {
        ContentResolver contentResolver = getContentResolver();
        String selection = "_id = "+id;
        Uri uri1 = Uri.parse("content://sms");
        Cursor cursor = contentResolver.query(uri1, null, selection, null, null);
        String phone = cursor.getString(cursor.getColumnIndex("address"));
        int type = cursor.getInt(cursor.getColumnIndex("type"));// 2 = sent, etc.
        String date = cursor.getString(cursor.getColumnIndex("date"));
        String body = cursor.getString(cursor.getColumnIndex("body"));
        Messages messages = new Messages(type, phone, body, date);
        smsList.add(messages);
    }
    private String getMmsText(String id) {
        Uri partURI = Uri.parse("content://mms/part/" + id);
        InputStream is = null;
        StringBuilder sb = new StringBuilder();
        try {
            is = getContentResolver().openInputStream(partURI);
            if (is != null) {
                InputStreamReader isr = new InputStreamReader(is, "UTF-8");
                BufferedReader reader = new BufferedReader(isr);
                String temp = reader.readLine();
                while (temp != null) {
                    sb.append(temp);
                    temp = reader.readLine();
                }
            }
        } catch (IOException e) {}
        finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {}
            }
        }
        return sb.toString();
    }
    private Bitmap getMmsImage(String _id) {
        Uri partURI = Uri.parse("content://mms/part/"+_id);
        InputStream is = null;
        Bitmap bitmap = null;
        try {
            is = getContentResolver().openInputStream(partURI);
            bitmap = BitmapFactory.decodeStream(is);
        } catch (IOException e) {}
        finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {}
            }
        }
        return bitmap;
    }
    private String getAddressNumber(int id) {
        String selectionAdd = new String("msg_id="+id);
        String uriStr = MessageFormat.format("content://mms/{0}/addr", id);
        Uri uriAddress = Uri.parse(uriStr);
        Cursor cAdd = getContentResolver().query(uriAddress, null,
                selectionAdd, null, null);
        String name = null;
        if (cAdd.moveToFirst()) {
            do {
                String number = cAdd.getString(cAdd.getColumnIndex("address"));
                if (number != null) {
                    try {
                        Long.parseLong(number.replace("-", ""));
                        name = number;
                    } catch (NumberFormatException nfe) {
                        if (name == null) {
                            name = number;
                        }
                    }
                }
            } while (cAdd.moveToNext());
        }
        if (cAdd != null) {
            cAdd.close();
        }
        return name;
    }
}
