package com.example.sms.models;

public class TaiKhoan {
    private int IdTK;
    private String TenTK;
    private String TenNH;
    private String Tien;

    public TaiKhoan(int idTK, String tenTK, String tenNH, String tien) {
        IdTK = idTK;
        TenTK = tenTK;
        TenNH = tenNH;
        Tien = tien;
    }

    public int getIdTK() {
        return IdTK;
    }

    public void setIdTK(int idTK) {
        IdTK = idTK;
    }

    public String getTenTK() {
        return TenTK;
    }

    public void setTenTK(String tenTK) {
        TenTK = tenTK;
    }

    public String getTenNH() {
        return TenNH;
    }

    public void setTenNH(String tenNH) {
        TenNH = tenNH;
    }

    public String getTien() {
        return Tien;
    }

    public void setTien(String tien) {
        Tien = tien;
    }
}
