package com.example.sms.models;

public class MMSPart {
    String partId;
    String type;



    public MMSPart(String partId, String type) {
        this.partId = partId;
        this.type = type;
    }

    public String getPartId() {
        return partId;
    }

    public void setPartId(String partId) {
        this.partId = partId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
