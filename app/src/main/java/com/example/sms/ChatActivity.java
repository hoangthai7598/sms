package com.example.sms;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sms.Adapter.ChatAdapter;
import com.example.sms.models.Messages;
import com.klinker.android.send_message.ApnUtils;
import com.klinker.android.send_message.Message;
import com.klinker.android.send_message.Transaction;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class ChatActivity extends AppCompatActivity {
    private static final int MY_PERMISSION_REQUEST_SEND_SMS =0;
    private  static  final int MY_PERMISSION_REQUEST_SEND_MMS =3;
    private  static  final int MY_PERMISSION_CHANGE_NETWORK_STATE =4;
    private static final int  MY_PERMISSION_WRITE_EXTERNAL_STORAGE = 5;
    private static final int  MY_PERMISSION_READ_PHONE_STATE = 6;
    private static final int  MY_PERMISSION_READ_SMS = 7;
    private static final int  MY_PERMISSION_RECEIVE_SMS = 8;
    private Settings settings;
    private static ChatActivity inst1;
    public static boolean active = false;
    public static ChatActivity instance()
    {
        return inst1;
    }
    public static final int PICK_IMAGE = 2;
    private  final int REQUEST_CALL = 1;
    String displayName,address;
    Toolbar toolbar;
    TextView txt_name;
    RecyclerView recycler_chats;
    EditText txt_chat;
    ImageButton btn_send;
    ChatAdapter chatAdapter;
    List<Messages> messagesList;
    private EditText searchTXT;
    CircleImageView btn_Pic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);


        //quyen nhan tin
        initSettings();
        if((ContextCompat.checkSelfPermission(this,Manifest.permission.READ_SMS)!=PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this,Manifest.permission.SEND_SMS)!=PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this,Manifest.permission.RECEIVE_SMS)!=PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this,Manifest.permission.RECEIVE_MMS)!=PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED)||
                (ContextCompat.checkSelfPermission(this,Manifest.permission.READ_PHONE_STATE)!=PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this,Manifest.permission.CHANGE_NETWORK_STATE)!=PackageManager.PERMISSION_GRANTED)
        )
        {
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.SEND_SMS))
            {

            }
            else
            {
                ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.SEND_SMS},MY_PERMISSION_REQUEST_SEND_SMS);
                ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.SEND_RESPOND_VIA_MESSAGE},MY_PERMISSION_REQUEST_SEND_MMS);
                ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.CHANGE_NETWORK_STATE},MY_PERMISSION_CHANGE_NETWORK_STATE);
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_PHONE_STATE},MY_PERMISSION_READ_PHONE_STATE);
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},MY_PERMISSION_WRITE_EXTERNAL_STORAGE);
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_SMS},MY_PERMISSION_READ_SMS);
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.RECEIVE_SMS},MY_PERMISSION_RECEIVE_SMS);
            }
        }

        Intent intent = getIntent();
        address = intent.getStringExtra("address");
        displayName = intent.getStringExtra("displayName");

        toolbar = findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);
        //tao thanh quay lai
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        inst1 = this;
        toolbar.setTitle("");
        txt_name = findViewById(R.id.txt_name);
        if(displayName.equals("none")){
            txt_name.setText(address);
        }
        else {
            txt_name.setText(displayName);
        }
        txt_chat = findViewById(R.id.txt_chat);
        recycler_chats = findViewById(R.id.recycler_chats);
        btn_send = findViewById(R.id.btn_send);

        messagesList = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        layoutManager.setStackFromEnd(true);
        recycler_chats.setLayoutManager(layoutManager);
        inst1 = this;
        getMessage();
        //recycler_chats.smoothScrollToPosition(recycler_chats.getAdapter().getItemCount());
        final String chat = txt_chat.getText().toString().trim();

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(txt_chat.getText().toString().trim())){
                    SmsManager smsManager = SmsManager.getDefault();
                    if(chat.length() >= 150)
                    {
                        ArrayList<String> parts =smsManager.divideMessage(chat);
                        int numParts = parts.size();
                        for(int i=0;i<numParts;i++)
                        {
                            smsManager.sendMultipartTextMessage(address, null, parts, null, null);
                        }
                        Messages newMsg = new Messages(2, address, txt_chat.getText().toString().trim(), String.valueOf(System.currentTimeMillis()));
                        messagesList.add(newMsg);
                        chatAdapter.notifyDataSetChanged();
                        recycler_chats.smoothScrollToPosition(recycler_chats.getAdapter().getItemCount());
                        if(displayName.equals("none"))
                            Toast.makeText(ChatActivity.this, "Tin nhắn được nhận bởi "+address, Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(ChatActivity.this, "Tin nhắn được nhận bởi "+ displayName+"("+address+")", Toast.LENGTH_SHORT).show();
                        txt_chat.setText("");
                    }
                    else
                        {
                        smsManager.sendTextMessage(address, null, txt_chat.getText().toString().trim(), null, null);
                        Messages newMsg = new Messages(2, address, txt_chat.getText().toString().trim(), String.valueOf(System.currentTimeMillis()));
                        ContentValues values = new ContentValues();
                        values.put("type",2);
                        values.put("body",txt_chat.getText().toString().trim());
                        values.put("address",address);
                        values.put("date",String.valueOf(System.currentTimeMillis()));
                        getContentResolver().insert(Uri.parse("content://sms"),values);
                        messagesList.add(newMsg);
                        chatAdapter.notifyDataSetChanged();
                        if(displayName.equals("none"))
                            Toast.makeText(ChatActivity.this, "Tin nhắn được nhận bởi "+address, Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(ChatActivity.this, "Tin nhắn được nhận bởi "+ displayName+"("+address+")", Toast.LENGTH_SHORT).show();
                        recycler_chats.smoothScrollToPosition(recycler_chats.getAdapter().getItemCount());
                        txt_chat.setText("");
                    }
                }
            }
        });
        btn_Pic = (CircleImageView) findViewById(R.id.btn_pic);
        btn_Pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });
    }
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        MenuItem menuItem = menu.findItem(R.id.it_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint("Nhập từ bạn muốn tìm");


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                //messagesList.clear();
                //getMessageByKeyWord(newText);
                chatAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if(item.getItemId() == R.id.it_call)
        {
            makeCall();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode == PICK_IMAGE)
//        {
//            if(resultCode == Activity.RESULT_OK)
//            {
//                Uri uri = data.getData();
//                Intent shareIntent = new Intent();
//                shareIntent.setAction(Intent.ACTION_SEND);
//                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
//                shareIntent.putExtra("address",address);
//                shareIntent.setType("image/jpeg");
//                startActivity(Intent.createChooser(shareIntent, null));
//            }
//        }
        if(requestCode == PICK_IMAGE)
        {
            if(resultCode == Activity.RESULT_OK)
            {
//                final Bundle extras = data.getExtras();
//                if (extras != null) {
//                    //Get image
//                    Bitmap pic = extras.getParcelable("data");
//                    sendImg(pic);
//                    Toast.makeText(this, "cặc", Toast.LENGTH_SHORT).show();
//                }
                // Let's read picked image data - its URI
                if(data!=null)
                {
                    Uri pickedImage = data.getData();
                    // Let's read picked image path using content resolver
                    String[] filePath = { MediaStore.Images.Media.DATA };
                    Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
                    cursor.moveToFirst();
                    String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);

                    // Do something with the bitmap


                    // At the end remember to close the cursor or you will end with the RuntimeException!
                    cursor.close();
                    sendImg(bitmap);
                }
            }
        }
    }

    private void makeCall()
    {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.CALL_PHONE},REQUEST_CALL);
        }else {
            String dial = "tel:" +address;
            startActivity(new Intent(Intent.ACTION_CALL,Uri.parse(dial)));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==REQUEST_CALL)
        {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                makeCall();
            }
        }
        else if(requestCode == MY_PERMISSION_REQUEST_SEND_SMS)
        {
            //check length of result grater than 0 and equal permission granted
            if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
            {
                //Toast.makeText(this, "Access", Toast.LENGTH_SHORT).show();
            }else{
                //Toast.makeText(this, "Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getMessageByKeyWord(String s)
    {
        //dung content provider
        messagesList.clear();
        //cursor class co api cho phep ung dung doc thong tin duoi dang tung cot
        Cursor cur = getContentResolver().query(Uri.parse("content://sms"), null, "body LIKE '%" + s + "%' AND address like'%"+address+"%'", null, "date asc");
        int indexBody = cur.getColumnIndex("body");
        int indexAddress = cur.getColumnIndex("address");
        int indexType = cur.getColumnIndex("type");
        int indexDate = cur.getColumnIndex("date");

        //kiem tra xem co tin nhan khong,neu khong co thi khong lam gi
        if (indexBody < 0 || !cur.moveToFirst() || indexType < 0) return;
        //neu tin nhan khong trong thi lay het
        do {
            int type = cur.getInt(indexType);
            String body = cur.getString(indexBody);
            String address = cur.getString(indexAddress);
            String date = cur.getString(indexDate);

            Messages messages = new Messages(type, address, body, date);

            messagesList.add(messages);

        } while (cur.moveToNext());
        chatAdapter = new ChatAdapter(this, messagesList);
        recycler_chats.setAdapter(chatAdapter);
        chatAdapter.notifyDataSetChanged();
    }
    public void getMessage()
    {
        messagesList.clear();
        Cursor cur = getContentResolver().query(Uri.parse("content://sms"), null, "address LIKE '%" + address + "%'", null, "date asc");
        int indexBody = cur.getColumnIndex("body");
        int indexAddress = cur.getColumnIndex("address");
        int indexType = cur.getColumnIndex("type");
        int indexDate = cur.getColumnIndex("date");

        if (indexBody < 0 || !cur.moveToFirst() || indexType < 0) return;
        do {
            int type = cur.getInt(indexType);
            String body = cur.getString(indexBody);
            String address = cur.getString(indexAddress);
            String date = cur.getString(indexDate);

            Messages messages = new Messages(type, address, body, date);

            messagesList.add(messages);

        } while (cur.moveToNext());
        chatAdapter = new ChatAdapter(this, messagesList);
        recycler_chats.setAdapter(chatAdapter);
        chatAdapter.notifyDataSetChanged();
    }
    public void updateMess(String Address,String MessBody)
    {
        Messages newMsg = new Messages(1, Address, MessBody, String.valueOf(System.currentTimeMillis()));
        messagesList.add(newMsg);
        chatAdapter.notifyDataSetChanged();
        recycler_chats.smoothScrollToPosition(recycler_chats.getAdapter().getItemCount());
    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;
        //inst1 = this;
    }

    @Override
    protected void onStop() {
        super.onStop();
        //active = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        active=false;
    }

    public String getAddress()
    {
        Intent intent = getIntent();
        address = intent.getStringExtra("address");
        return address;
    }
    public void Insert(ContentValues values)
    {
//        values.put("type",1);
//        values.put("date",String.valueOf(System.currentTimeMillis()));
//        values.put("body",Body);
//        values.put("address",Address);
        Uri uri = getContentResolver().insert(Uri.parse("content://sms"),values);
    }
    // setting
    private void initSettings() {
        settings = Settings.get(this);

        if (TextUtils.isEmpty(settings.getMmsc()) &&
                Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            initApns();
        }
    }
    private void initApns() {
        ApnUtils.initDefaultApns(this, new ApnUtils.OnApnFinishedListener() {
            @Override
            public void onFinished() {
                settings = Settings.get(ChatActivity.this, true);
            }
        });
    }
    public void sendImg(final Bitmap pic)
    {
//        final String phone = txt_phone.getText().toString().trim();
//        final String address = phone.replaceAll(" ","");
        Intent intent = getIntent();
        address = intent.getStringExtra("address");
        final String chat = txt_chat.getText().toString();
//        if(!TextUtils.isEmpty(chat) && !TextUtils.isEmpty(phone)) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                com.klinker.android.send_message.Settings sendSettings = new com.klinker.android.send_message.Settings();
                sendSettings.setMmsc(settings.getMmsc());
                sendSettings.setProxy(settings.getMmsProxy());
                sendSettings.setPort(settings.getMmsPort());
                sendSettings.setUseSystemSending(true);
                Transaction transaction = new Transaction(ChatActivity.this, sendSettings);

                Message message = new Message(chat, address);
                message.setImage(pic);
                transaction.sendNewMessage(message, Transaction.NO_THREAD_ID);
            }
        }).start();
        txt_chat.setText("");
        Toast.makeText(this, "đã gửi", Toast.LENGTH_SHORT).show();
//        }else Toast.makeText(this, "Nhập tin nhắn hoặc SĐT", Toast.LENGTH_SHORT).show();
    }
}
